'use strict'
const fs = require('fs')
const paths = require('./paths')

const NODE_ENV = process.env.NODE_ENV

if (!NODE_ENV) {
  throw new Error(
    'The NODE_ENV environment variable is required but was not specified.'
  )
}

const dotenvFiles = [
  `${paths.dotenv}.${NODE_ENV}.local`,
  `${paths.dotenv}.${NODE_ENV}`,
  // Don't include `.env.local` for `test` environment
  // since normally you expect tests to produce the same
  // results for everyone
  NODE_ENV !== 'test' && `${paths.dotenv}.local`,
  paths.dotenv
].filter(Boolean)

dotenvFiles.forEach(dotenvFile => {
  if (fs.existsSync(dotenvFile)) {
    require('dotenv-expand')(
      require('dotenv').config({
        path: dotenvFile
      })
    )
  }
})

const env = {
  STATS_API: process.env.STATS_API || 'https://api-dev.flavorwiki.com/v1/',
  STATS_SERVER_API: process.env.STATS_SERVICE_API || 'http://localhost:4100',
  EXPORTS_API: process.env.EXPORTS_API || 'https://localhost:1437/',
  APP_URL: process.env.APP_URL || 'https://localhost:3000/'
}

module.exports = env
