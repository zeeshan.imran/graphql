require('./env')

const serverOptions = {
  cors: {
    origin: [
      'https://app.flavorwiki.com',
      'https://chr-hansen.flavorwiki.com',
      'https://blc.flavorwiki.com',
      'https://staging.flavorwiki.com',
      'https://staging1.flavorwiki.com',
      'https://staging2.flavorwiki.com',
      'https://staging3.flavorwiki.com',
      'https://staging4.flavorwiki.com',
      'https://staging5.flavorwiki.com',
      'https://staging6.flavorwiki.com',
      'https://staging7.flavorwiki.com',
      'https://staging8.flavorwiki.com',
      'https://staging9.flavorwiki.com',
      'https://staging10.flavorwiki.com'
    ],
    methods: ['GET', 'POST'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Impersonate']
  },
  port: process.env.PORT || 8000,
  endpoint: '/graphql',
  playground: false,
  subscriptions: '/subscriptions',
  bodyParserOptions: { limit: '100mb' }
}

if (process.env.PLAYGROUND === 'true') {
  serverOptions.playground = '/playground'
}

module.exports = serverOptions
