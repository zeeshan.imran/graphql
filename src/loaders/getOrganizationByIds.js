const { map, find } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../utils/getApiHeaders')

const getOrganizationByIds = async (headers, ids) => {
  const query = JSON.stringify({
    id: ids
  })

  const organizations = await rp({
    method: 'GET',
    headers: getApiHeaders(headers),
    uri: `${process.env.API_URL}/organization?where=${query}`,
    json: true
  })

  return map(id => find(o => o.id === id, organizations), ids)
}

module.exports = getOrganizationByIds
