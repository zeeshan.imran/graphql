const { map, find } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../utils/getApiHeaders')

const getProductByIds = async (headers, ids) => {
  const query = JSON.stringify({
    id: ids
  })

  const product = await rp({
    method: 'GET',
    headers: getApiHeaders(headers),
    uri: `${process.env.API_URL}/product?where=${query}&populate=false`,
    json: true
  })

  return map(id => find(o => o.id === id, product), ids)
}

module.exports = getProductByIds
