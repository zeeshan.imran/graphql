const { map, find } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../utils/getApiHeaders')

const getSurveyCoverByIds = async (headers, ids) => {
  const questions = await rp({
    method: 'POST',
    headers: getApiHeaders(headers),
    uri: `${process.env.API_URL}/survey/getCoversOfSurveys`,
    json: true,
    body: {
      surveys: ids
    }
  })

  return map(
    id =>
      (find(o => o.survey === id, questions) || { coverPhoto: null }).coverPhoto,
    ids
  )
}

module.exports = getSurveyCoverByIds
