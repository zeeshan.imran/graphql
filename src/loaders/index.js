const DataLoader = require('dataloader')
const getOrganizationByIds = require('./getOrganizationByIds')
const getProductByIds = require('./getProductByIds')
const getUserByIds = require('./getUserByIds')
const getQuestionByIds = require('./getQuestionByIds')
const getSurveyCoverByIds = require('./getSurveyCoverByIds')

module.exports = function createLoaders ({ headers }) {
  return {
    productLoader: new DataLoader(ids => getProductByIds(headers, ids)),
    userLoader: new DataLoader(ids => getUserByIds(headers, ids)),
    organizationLoader: new DataLoader(ids =>
      getOrganizationByIds(headers, ids)
    ),
    questionLoader: new DataLoader(ids => getQuestionByIds(headers, ids)),
    surveyCoverLoader: new DataLoader(ids => getSurveyCoverByIds(headers, ids))
  }
}
