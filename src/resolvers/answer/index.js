const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    surveyAnswers: async (_, { enrollmentId }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'GET',
        uri: `${process.env.API_URL}/surveyAnswers/${enrollmentId}`,
        json: true
      })
      return res
    }
  }
}
