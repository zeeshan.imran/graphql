const rp = require('request-promise')
const env = require('../../config/env')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    getFileInfo: async (_root, { id }, { headers }) => {
      try {
        return await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          url: `${env.EXPORTS_API}/files/${id}/info`,
          json: true
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    getFileDownloadLink: async (_root, { id }, { headers }) => {
      try {
        return rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          url: `${env.EXPORTS_API}/files/${id}/link`,
          json: true
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    }
  }
}
