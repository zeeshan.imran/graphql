const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  OrganizationOrderField: {
    createdBy: 'createdBy.fullName',
    modifiedBy: 'modifiedBy.fullName'
  },
  Query: {
    organizations: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/organizations?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    countOrganizations: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/organizationsNumber?query=${query}`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error)
      }
    }
  },
  Mutation: {
    addOrganization: async (_, { name, uniqueName }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: {
            name,
            uniqueName
          },
          uri: `${process.env.API_URL}/organization/add`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    editOrganization: async (_, { name, id }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: {
            id: id,
            name: name
          },
          uri: `${process.env.API_URL}/organization/edit/${id}`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    deleteOrganization: async (_, { id }, { headers }) => {
      try {
        const res = await rp({
          method: 'DELETE',
          headers: getApiHeaders(headers),
          body: {
            id: id
          },
          uri: `${process.env.API_URL}/organization/${id}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    }
  }
}
