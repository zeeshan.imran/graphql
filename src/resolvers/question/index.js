const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

const QuestionSettingTypes = {
  'choose-multiple': 'MultipleQuestionSettings',
  'taster-name': 'TasterNameSettings',
  'matrix': 'MultipleQuestionSettings'
}

const QuestionSettingsDefault = {
  'choose-multiple': {
    minAnswerValues: null,
    maxAnswerValues: null
  },
  'taster-name': {
    minLength: null,
    maxLength: null,
    answerFormat: 'any',
    allowSpaces: true
  },
  'matrix': {
    minAnswerValues: 1,
    maxAnswerValues: null,
    chooseMultiple: false
  }
}

module.exports = {
  QuestionSettings: {
    __resolveType: obj => {
      return QuestionSettingTypes[obj.type]
    }
  },
  SkipFlow: {
    __resolveType: skipFlow => {
      return (
        {
          MATCH_OPTION: 'SkipOptionFlow',
          MATCH_PRODUCT: 'SkipProductFlow',
          MATCH_RANGE: 'SkipRangeFlow',
          MATCH_MULTIPLE_OPTION: 'SkipMultipleFlow',
          MATCH_MATRIX_OPTION: 'SkipMatrixFlow'
        }[skipFlow.type] || null
      )
    }
  },
  Question: {
    settings: question =>
      QuestionSettingTypes[question.typeOfQuestion] &&
      (question.settings || {
        ...QuestionSettingsDefault[question.typeOfQuestion],
        type: question.typeOfQuestion
      }),
    type: question => question.typeOfQuestion,
    required: question => question.requiredQuestion,
    chooseProductOptions: async (question, _args, { headers }) => {
      if (question.typeOfQuestion !== 'choose-product') {
        return null
      }

      const survey = await rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/${question.survey}`,
        json: true
      })

      return {
        minimumProducts: survey.minimumProducts,
        maximumProducts: survey.maximumProducts
      }
    },
    pairs: question => {
      return []
    }
  },
  Query: {
    question: async (_, { id }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/question/${id}?populate=false`,
        json: true
      })
      return res
    },
    questions: (_, { ids = [] }, { questionLoader }) => {
      return questionLoader.loadMany(ids)
    }
  },
  Mutation: {
    createQuestion: async (_, { input }, { headers }) => {
      try {
        // NOTE: STAGFW-455 tested
        const createdQuestion = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/question`,
          body: input,
          json: true
        })
        return createdQuestion
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateQuestion: async (_, { input }, { headers }) => {
      try {
        // NOTE: STAGFW-455 tested
        const { id, ...rest } = input
        const updatedQuestion = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/question/${id}`,
          body: rest,
          json: true
        })
        return updatedQuestion
      } catch (error) {
        throw new Error(error.error)
      }
    },
    deleteQuestion: async (_, { question }, { headers }) => {
      try {
        const deletedQuestion = await rp({
          headers: getApiHeaders(headers),
          method: 'DELETE',
          uri: `${process.env.API_URL}/question/${question}`,
          json: true
        })

        return deletedQuestion.id
      } catch (error) {
        throw new Error(error.error)
      }
    },
    replaceQuestions: async (_, { input: questions }, { headers }) => {
      try {
        const replacedQuestions = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/question/replaceQuestions`,
          body: { questions },
          json: true
        })

        return replacedQuestions
      } catch (error) {
        throw new Error(error.error)
      }
    }
  }
}
