const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    getChartsSettings: async (_, { surveyId }, { headers }) => {
      const res = await rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/settings/charts/${surveyId}`,
        json: true,
      })
      return res
    }
  },
  Mutation: {
    saveChartsSettings: async (_, { surveyId, chartsSettings }, { headers }
    ) => {
      const res = await rp({
        method: 'POST',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/settings/charts`,
        json: true,
        body: {
          surveyId,
          chartsSettings
        }
      })

      return res
    }
  }
}
