const rp = require('request-promise')
const env = require('../../config/env')

module.exports = {
  Query: {
    findSurveyAnalysisResults: async (_root, { jobGroup }) => {
      try {
        const rs = await rp({
          method: 'GET',
          uri: `${env.STATS_SERVER_API}/groupJobs/${jobGroup}`,
          json: true
        })
        return rs.map(({ data, ...other }) => ({
          ...other,
          ...data
        }))
      } catch (ex) {
        throw ex
      }
    },
    getAnalysisFile: async (_root, { id, file }) => {
      try {
        return await rp({
          method: 'GET',
          uri: `${env.STATS_SERVER_API}/job/${id}/${file}`,
          json: true
        })
      } catch (ex) {
        throw ex
      }
    }
  },
  Mutation: {
    createJobs: async (_root, { input }) => {
      try {
        const {
          question: questionId,
          analysisTypes,
          referenceQuestion,
          productNames,
          questionCriteria
        } = input

        const rs = await rp({
          method: 'POST',
          uri: `${env.STATS_SERVER_API}/${questionId}`,
          body: {
            analysisTypes,
            referenceQuestion,
            productNames,
            questionCriteria
          },
          json: true
        })
        return rs.jobGroup
      } catch (ex) {
        throw ex
      }
    }
  }
}
