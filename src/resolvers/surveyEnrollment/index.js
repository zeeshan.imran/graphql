const rp = require('request-promise')
const env = require('../../config/env')
const getApiHeaders = require('../../utils/getApiHeaders')
const tastingProfileData = require('./tastingProfileData')
const sliderProfileData = require('./sliderProfileData')
const submitAnswer = require('./submitAnswer')
const finishSurvey = require('./finishSurvey')
const rejectSurvey = require('./rejectSurvey')
const finishSurveyScreening = require('./finishSurveyScreening')

module.exports = {
  SurveyEnrollment: {
    survey: async enrollment => {
      const { survey } = enrollment
      // getting survey by ID
      return rp({
        method: 'GET',
        uri: `${process.env.API_URL}/survey/${survey}`,
        json: true
      })
    }
  },
  Query: {
    surveyEnrollments: async () => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment`,
        json: true
      })

      return res
    },
    surveyEnrollment: async (_, { id }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment/${id}`,
        json: true
      })

      return res
    },
    surveyEnrollementValidation: async (_, params, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${
            process.env.API_URL
          }/surveyenrollement/validation?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },

    surveyFunnel: async (_, params, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/surveyenrollement/funnel?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    tastingProfileData,
    sliderProfileData
  },
  Mutation: {
    saveRewards: async (_, { rewards, surveyEnrollment }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/saveRewards`,
        body: { rewards, surveyEnrollment },
        json: true
      })

      return res
    },
    setSelectedProducts: async (_, { surveyEnrollment, selectedProducts }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/selectedproducts`,
        body: {
          surveyEnrollment,
          selectedProducts
        },
        json: true
      })

      return res
    },
    setPaypalEmail: async (_, { surveyEnrollment, paypalEmail }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/paypalemail`,
        body: {
          surveyEnrollment,
          paypalEmail
        },
        json: true
      })
      return res
    },
    updateValidateSurveyEnrollement: async (_, { input }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: input,
          uri: `${process.env.API_URL}/surveyenrollement/validation/${
            input.id
          }`,
          json: true
        })

        const respondentId = input.id
        const surveyId = input.surveyId
        await rp({
          method: 'POST',
          uri: `${
            env.STATS_SERVER_API
          }/survey/${surveyId}/respondent/${respondentId}/validation`,
          body: {
            validation: input.validation === 'valid' ? 1 : 0
          },
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error)
      }
    },
    updateSurveyEnrolmentProductIncentiveStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${
            process.env.API_URL
          }/surveyenrollement/product-incentive-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateSurveyEnrolmentGiftCardIncentiveStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${
            process.env.API_URL
          }/surveyenrollement/gift-card-incentive-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateSurveyShareStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${
            process.env.API_URL
          }/surveyenrollement/survey-shares-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    submitForcedSignUp: async (
      _,
      { input: { surveyEnrollment, email, password, country } }
    ) => {
      try {
        const url = `${process.env.API_URL}/surveyenrollement/signuptosurvey`

        await rp({
          method: 'POST',
          uri: url,
          body: {
            surveyEnrollment,
            email,
            password,
            country
          },
          json: true
        })
        return true
      } catch (error) {
        throw new Error(error)
      }
    },
    submitAnswer,
    finishSurvey,
    rejectSurvey,
    finishSurveyScreening
  }
}
