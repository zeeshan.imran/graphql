const rp = require('request-promise')

const rejectSurvey = async (_, { input }) => {
  try {
    const { surveyEnrollment } = input

    const { survey: { id: surveyId } = {}, user } = await rp({
      method: 'GET',
      uri: `${process.env.API_URL}/surveyEnrollment/${surveyEnrollment}`,
      json: true
    })

    if (user && user.emailAddress) {
      rp({
        method: 'POST',
        uri: `${process.env.API_URL}/email`,
        body: {
          email: user.emailAddress,
          id: surveyId,
          type: 'survey-rejected',
          surveyEnrollment
        },
        json: true
      })
    }
 
    await rp({
      method: 'POST',
      uri: `${process.env.API_URL}/surveyenrollment/rejectSurvey`,
      body: {
        surveyEnrollment
      },
      json: true
    })
  } catch (error) {
    throw new Error(error.error)
  }
}

module.exports = rejectSurvey
