const { omit } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  UserOrderField: {
    organization: 'orgs.orderName'
  },
  User: {
    organization: async (user, _args, { organizationLoader, headers }) => {
      try {
        let organization = null
        if (user.organization && getApiHeaders(headers).Authorization) {
          organization =
            typeof user.organization === 'string'
              ? await organizationLoader.load(user.organization)
              : user.organization
        }
        return organization
      } catch (error) {
        throw new Error(error.error)
      }
    }
  },
  Query: {
    countUsers: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/user/count?query=${query}`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    users: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/users?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    user: async (_, { id }, { headers }) => {
      const apiHeaders = getApiHeaders(headers)
      const res = await rp({
        method: 'GET',
        headers: apiHeaders,
        uri: `${process.env.API_URL}/user/${id}`,
        json: true
      })

      return res
    },
    me: async (_, { impersonate = true }, { headers }) => {
      const apiHeaders = impersonate
        ? getApiHeaders(headers)
        : getApiHeaders(omit(['impersonate'], headers))
      try {
        const res = await rp({
          method: 'GET',
          headers: apiHeaders,
          uri: `${process.env.API_URL}/user/me`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    }
  },
  Mutation: {
    addUser: async (_, { input }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: {
            emailAddress: input.emailAddress,
            fullName: input.fullName,
            type: input.type,
            organization: input.organization,
            isSuperAdmin: input.isSuperAdmin,
            isTaster: input.isTaster,
            id: input.id
          },
          uri: `${process.env.API_URL}/user/add`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    editUser: async (_, { input }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: input,
          uri: `${process.env.API_URL}/user/edit/${input.id}`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    deleteUser: async (_, { id }, { headers }) => {
      try {
        const res = await rp({
          method: 'DELETE',
          headers: getApiHeaders(headers),
          body: {
            id: id
          },
          uri: `${process.env.API_URL}/user/${id}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    createTasterAccount: async (_, { input }, { headers }) => {
      try {
        const user = await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/user/createtasteraccount`,
          body: input,
          json: true
        })

        headers.Authorization = `Bearer ${user.token}`

        return user
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateTasterAccount: async (_, { input }, { headers }) => {
      try {
        if (input.firstName) {
          input['fullName'] = input.firstName + ' ' + input.lastName
        }
        const user = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/user/updatetasteraccount`,
          body: input,
          json: true
        })
        return user
      } catch (error) {
        throw new Error(error.error)
      }
    },
    requestAccount: async (
      _,
      {
        input: {
          email,
          userType,
          firstName,
          lastName,
          companyName,
          phoneNumber
        }
      }
    ) => {
      try {
        await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/user/requestaccount`,
          body: {
            email,
            userType,
            firstName,
            lastName,
            companyName,
            phoneNumber
          },
          json: true
        })
        return true
      } catch (error) {
        if (error.message.includes('Taster Account')) {
          throw new Error(error.error)
        }
        throw new Error(error.error && error.error.code)
      }
    },
    loginToSurvey: async (
      _,
      {
        email,
        survey,
        referral,
        isUserLoggedIn,
        password,
        country,
        browserInfo
      },
      { headers }
    ) => {
      try {
        const url = isUserLoggedIn
          ? `${process.env.API_URL}/user/logintosurveywithauth`
          : `${process.env.API_URL}/user/logintosurvey`

        const res = await rp({
          method: 'POST',
          uri: url,
          headers: isUserLoggedIn ? getApiHeaders(headers) : null,
          body: {
            email,
            survey,
            referral,
            password,
            country,
            browserInfo
          },
          json: true
        })

        const selectedProducts = res.selectedProducts || []

        return { ...res, selectedProducts }
      } catch (error) {
        throw new Error(error)
      }
    },
    loginUser: async (_, { email, password }, { headers }) => {
      try {
        const user = await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/login`,
          body: {
            email,
            password
          },
          json: true
        })

        headers.Authorization = `Bearer ${user.token}`

        return user
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateUserPassword: async (
      _,
      { currentPassword, newPassword },
      { headers }
    ) => {
      try {
        const changePassword = await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/update-user-password`,
          headers: getApiHeaders(headers),
          body: {
            currentPassword,
            newPassword
          },
          json: true
        })

        return changePassword
      } catch (error) {
        throw new Error(error.error)
      }
    },
    forgotPassword: async (_, { email }) => {
      try {
        await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/user/forgotpassword`,
          body: {
            email
          },
          json: true
        })

        return true
      } catch (error) {
        throw new Error(error.error)
      }
    },
    resetPassword: async (_, { input }) => {
      try {
        const { email, password, token } = input
        await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/user/resetpassword`,
          body: {
            email,
            password,
            token
          },
          json: true
        })

        return true
      } catch (error) {
        throw new Error(error.error)
      }
    },
    completeProfile: async (_, { input }, { headers }) => {
      try {
        const { id, fullName, birthYear, nationality, gender } = input

        const updatedUser = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/user/${id}`,
          body: {
            fullName,
            birthYear,
            nationality,
            gender,
            initialized: true
          },
          json: true
        })

        return {
          ...updatedUser,
          organization: updatedUser.organization && updatedUser.organization.id
        }
      } catch (error) {
        throw new Error(error.error)
      }
    },
    isUserRegistered: async (_, { email }) => {
      try {
        await rp({
          method: 'POST',
          uri: `${process.env.API_URL}/user/isregistered`,
          body: {
            email
          },
          json: true
        })

        return true
      } catch (error) {
        throw new Error(error)
      }
    }
  }
}
