const { makeExecutableSchema } = require('graphql-tools')
const merge = require('lodash.merge')

const JSON = require('./types/JSON')
const Survey = require('./types/Survey')
const Question = require('./types/Question')
const Organization = require('./types/Organization')
const Answer = require('./types/Answer')
const Product = require('./types/Product')
const User = require('./types/User')
const SurveyEnrollment = require('./types/SurveyEnrollment')
const ImageUpload = require('./types/ImageUpload')
const Brand = require('./types/Brand')
const QrCode = require('./types/QrCode')
const File = require('./types/File')
const Settings = require('./types/Settings')
const SurveyAnalysis = require('./types/SurveyAnalysis')

const SurveyResolvers = require('./resolvers/survey')
const QuestionResolvers = require('./resolvers/question')
const OrganizationResolvers = require('./resolvers/organization')
const AnswerResolvers = require('./resolvers/answer')
const UserResolvers = require('./resolvers/user')
const ImageUploadResolvers = require('./resolvers/imageUpload')
const SurveyEnrollmentResolvers = require('./resolvers/surveyEnrollment')
const JSONResolvers = require('./resolvers/json')
const BrandResolvers = require('./resolvers/brand')
const ProductResolvers = require('./resolvers/product')
const QrCodeResolvers = require('./resolvers/qrCode')
const FileResolvers = require('./resolvers/file')
const SettingsResolvers = require('./resolvers/settings')
const SurveyAnalysisResolvers = require('./resolvers/surveyAnalysis')

const Root = /* GraphQL */ `
  # The dummy queries and mutations are necessary because
  # graphql-js cannot have empty root types and we only extend
  # these types later on
  # Ref: apollographql/graphql-tools#293
  scalar Object
  type Query {
    dummy: String
  }
  type Mutation {
    dummy: String
  }
  type Subscription {
    dummy: String
  }
  enum OrderDirection {
    ascend
    descend
  }
  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`

const RootResolvers = {
  OrderDirection: {
    ascend: 'ASC',
    descend: 'DESC'
  }
}

const resolvers = merge(
  RootResolvers,
  SurveyResolvers,
  QuestionResolvers,
  UserResolvers,
  ImageUploadResolvers,
  AnswerResolvers,
  JSONResolvers,
  SurveyEnrollmentResolvers,
  BrandResolvers,
  ProductResolvers,
  OrganizationResolvers,
  QrCodeResolvers,
  FileResolvers,
  SettingsResolvers,
  SurveyAnalysisResolvers
)

module.exports = makeExecutableSchema({
  typeDefs: [
    Root,
    Survey,
    Question,
    Product,
    User,
    SurveyEnrollment,
    ImageUpload,
    JSON,
    Answer,
    Brand,
    Organization,
    QrCode,
    File,
    Settings,
    SurveyAnalysis
  ],
  resolvers
})
