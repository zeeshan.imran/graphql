const Answer = /* GraphQL */ `
  type Answer {
    id: ID!
    value: [Object]!
    pairQuestion: Pair
    startedAt: String!
    timeToAnswer: Int
    enrollment: SurveyEnrollment
    question: Question
    product: Product
  }
  extend type Query {
    surveyAnswers(enrollmentId: [String]):JSON
  }
`

module.exports = Answer
