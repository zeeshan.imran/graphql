const Brand = /* GraphQL */ `
  type Brand {
    id: ID!
    logo: String
    name: String
  }

  extend type Query {
    brand(id: ID): Brand
    brands(ids: [ID]): [Brand]
  }
`

module.exports = Brand
