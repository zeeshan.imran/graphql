const ImageUpload = /* GraphQL */ `
  type FileLink {
    signedUrl: String
    fileLink: String
  }
  extend type Query {
    getSignedUrl(type: String): FileLink
  }
`

module.exports = ImageUpload
