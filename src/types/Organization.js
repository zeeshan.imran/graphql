const Organization = /* GraphQL */ `
  type OrganizationUserDetails {
    emailAddress: String
    fullName: String
    id: ID
  }

  type Organization {
    id: ID!
    name: String
    uniqueName: String!
    members: [User]
    createdBy: OrganizationUserDetails
    modifiedBy: OrganizationUserDetails
    usersNumber: Int
  }

  enum OrganizationOrderField {
    name
    createdBy
    modifiedBy
    usersNumber
  }

  input OrganizationsInput {
    keyword: String
    #
    orderBy: OrganizationOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
    type: [String!]
  }

  input CountOrganizationsInput {
    keyword: String
    type: [String!]
  }

  extend type Query {
    organizations(input: OrganizationsInput!): [Organization]
    countOrganizations(input: CountOrganizationsInput!): Int
  }

  extend type Mutation {
    addOrganization(name: String!, uniqueName: String!): Boolean
    editOrganization(name: String!, id: ID!): Organization
    deleteOrganization(id: ID!): Boolean
  }
`
module.exports = Organization
