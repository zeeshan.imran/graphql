const Question = /* GraphQL */ `
  type Question {
    id: ID!
    displayOn: String
    type: String!
    required: Boolean
    prompt: String
    secondaryPrompt: String
    range: Range
    options: [Options]
    settings: QuestionSettings
    chooseProductOptions: ChooseProductOptions
    pairsOptions: PairsOptions
    numericOptions: NumericOptions
    pairs: [Pair]
    addCustomOption: Boolean
    relatedQuestions: JSON
    order: Int
    nextQuestion: ID
    likingQuestion: ID
    chartTitle: String
    chartTopic: String
    chartType: String
    sliderOptions: SliderOptions
    matrixOptions: MatrixOptions
    skipFlow: SkipFlow
    region: String
    optionDisplayType: OptionDisplayTypes
    showProductImage: Boolean,
  }

  enum OptionDisplayTypes {
    labelOnly
    imageOnly
    imageAndLabel
  }

  type Options {
    label: String
    value: String
    skipTo: ID
    isOpenAnswer: Boolean
    image200: String
    image800: String
  }

  type MultipleQuestionSettings {
    minAnswerValues: Int
    maxAnswerValues: Int
    chooseMultiple: Boolean
  }

  type TasterNameSettings {
    minLength: Int
    maxLength: Int
    answerFormat: String
    allowSpaces: Boolean
  }

  union QuestionSettings = MultipleQuestionSettings | TasterNameSettings

  type Slider {
    label: String
  }

  type SliderOptions {
    sliders: [Slider]
    hasFollowUpProfile: Boolean
    profilePrompt: String
  }
  type Matrix {
    label: String
    id: String
  }

  type MatrixOptions {
    question: [Matrix]
   
  }

  type ChooseProductOptions {
    minimumProducts: Int
    maximumProducts: Int
  }

  enum SkipFlowType {
    MATCH_OPTION
    MATCH_PRODUCT
    MATCH_RANGE
    MATCH_MULTIPLE_OPTION
    MATCH_MATRIX_OPTION
  }

  union SkipFlow =
      SkipOptionFlow
    | SkipProductFlow
    | SkipRangeFlow
    | SkipMultipleFlow
    | SkipMatrixFlow

  type SkipOptionFlow {
    type: SkipFlowType
    rules: [SkipOptionRule]
  }

  type SkipOptionRule {
    index: Int
    skipTo: ID
  }

  type SkipMultipleFlow {
    type: SkipFlowType
    rules: [SkipMultipleOptionRule]
  }

  type SkipMatrixFlow {
    type: SkipFlowType
    rules: [SkipMatrixOptionRule]
  }

  type SkipMultipleOptionRule {
    options: [SkipMultipleObject]
    skipOptions: [SkipMultipleObject]
    answers: [String]
    skipTo: ID
    type: String
  }

  type SkipMatrixOptionRule {
    options: [SkipMatrixObject]
    skipOptions: [SkipMultipleObject]
    answers: [String]
    skipTo: ID
    type: String
  }

  type SkipMultipleObject {
    value: String
    label: String
  }

  type SkipMatrixObject {
    id: String
    label: String
  }

  type SkipProductFlow {
    type: SkipFlowType
    rules: [SkipProductRule]
  }

  type SkipProductRule {
    productId: ID
    skipTo: ID
  }

  type SkipRangeFlow {
    type: SkipFlowType
    rules: [SkipRangeRule]
  }

  type SkipRangeRule {
    minValue: Float
    maxValue: Float
    skipTo: ID
  }

  type PairsOptions {
    minPairs: Int
    maxPairs: Int
    customFlavorEnabled: Boolean
    oneFlavorNotPresentEnabled: Boolean
    neitherFlavorPresentEnabled: Boolean
    bothFlavorsPresentEnabled: Boolean
    elementsInPairs: [Object]
  }

  type Pair {
    id: ID!
    leftAttribute: JSON
    rightAttribute: JSON
    leftAttributeCounter: Int
    rightAttributeCounter: Int
    answers: [String]
    image800: String
  }

  type Range {
    min: Float
    max: Float
    step: Float
    labels: [String]
    isInOneRow: Boolean
    marks: [rangeMarks]
  }

  type rangeMarks {
    label: String
    value: Float
    isMajorUnit: Boolean
  }

  input QuestionInput {
    id: ID
    type: String
    prompt: String
    secondaryPrompt: String
    displayOn: String
    order: Int
    survey: ID
    range: JSON
    options: JSON
    chooseProductOptions: ChooseProductOptionsInput
    addCustomOption: Boolean
    requiredQuestion: Boolean
    settings: JSON
    pairsOptions: JSON
    likingQuestion: ID
    relatedQuestions: [ID]
    pairs: [Object]
    chartTitle: String
    chartTopic: String
    chartType: String
    sliderOptions: JSON
    matrixOptions:JSON
    region: String
    numericOptions: JSON
    hasPaymentOptions: Boolean
    optionDisplayType: OptionDisplayTypes
    showProductImage: Boolean
  }

  input LinkQuestionInput {
    from: ID!
    to: ID
  }

  input ChooseProductOptionsInput {
    minimumProducts: Int
    maximumProducts: Int
  }

  extend type Query {
    question(id: ID): Question
    questions(ids: [ID]): [Question]
  }

  input ReplaceQuestionInput {
    clientGeneratedId: ID
    id: ID
    type: String
    prompt: String
    secondaryPrompt: String
    displayOn: String
    order: Int
    survey: ID
    range: JSON
    options: JSON
    settings: JSON
    chooseProductOptions: ChooseProductOptionsInput
    addCustomOption: Boolean
    requiredQuestion: Boolean
    pairsOptions: JSON
    likingQuestion: ID
    numericOptions: JSON
    relatedQuestions: [ID]
    pairs: [Object]
    chartTitle: String
    chartTopic: String
    chartType: String
    sliderOptions: JSON
    matrixOptions: JSON
    skipFlow: JSON
    region: String
    hasPaymentOptions: Boolean
    optionDisplayType: OptionDisplayTypes
    showProductImage: Boolean
  }

  type ReplaceQuestionPayload {
    clientGeneratedId: ID
    question: Question!
  }

  extend type Mutation {
    createQuestion(input: QuestionInput): Question
    updateQuestion(input: QuestionInput): Question
    replaceQuestions(input: [ReplaceQuestionInput!]): [ReplaceQuestionPayload]
    deleteQuestion(question: ID): ID
  }

  type NumericOptions {
    min: Float
    max: Float
    decimalNumbers: Int
  }
`

module.exports = Question
