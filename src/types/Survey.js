const Survey = /* GraphQL */ `
  type CustomButtons {
    continue: String
    start: String
    next: String
    skip: String
  }

  type AutoAdvanceSettings {
    active: Boolean
    debounce: String
    hideNextButton: Boolean
  }
  type PdfFooterSettings {
    active: Boolean
    footerNote: String
  }

  enum ProductDisplayTypes {
    reverse
    permutation
    forced
    none
  }

  type Survey {
    id: ID!
    name: String
    coverPhoto: String
    creator: User
    lastModifier: User
    uniqueName: String
    state: String
    minimumProducts: Int
    maximumProducts: Int
    products: [Product]
    instructionsText: JSON
    instructionSteps: [String]
    screeningQuestions: [Question]
    setupQuestions: [Question]
    productsQuestions: [Question]
    finishingQuestions: [Question]
    paymentQuestions: [Question]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    authorizationType: String
    settings: JSON
    enabledEmailTypes: [String!]!
    emails: Emails
    surveyLanguage: String
    customButtons: CustomButtons
    exclusiveTasters: [User]
    sharedStatsUsers: [User]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    showOnTasterDashboard: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: TastingNotes
    country: String
    availableIn: String
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    autoAdvanceSettings: AutoAdvanceSettings
    pdfFooterSettings: PdfFooterSettings
    sharingButtons: Boolean
    validatedData: Boolean
    stats(productIds: [String!], questionFilters: [Object], sync: Boolean, enrollmentId: [String]): JSON
    savedRewards: JSON
    maxProductStatCount: Int
    owner: String
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [Survey]
    screeners: [Survey!]!
    createdAt: Float
    updatedAt: Float
  }

  type SurveyShares {
    id: ID
    email: String
    amount: String
    currency: String
    referral: ID
    refferedIds: [ID]
  }

  type Emails {
    surveyWaiting: Email
    surveyRejected: Email
    surveyCompleted: Email
  }

  type TastingNotes {
    tastingId: String
    tastingLeader: String
    customer: String
    country: String
    dateOfTasting: String
    otherInfo: String
  }

  type Email {
    subject: String
    html: String
    text: String
  }

  enum SurveyState {
    draft
    active
    suspended
    deprecated
  }

  enum SurveyOrderField {
    name
  }

  input SurveysInput {
    keyword: String
    state: SurveyState
    #
    orderBy: SurveyOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
    omitScreeners: Boolean
    omitUsedTastings: ID
    authorizationType: String
    showAllSurveys: Boolean
  }

  input CountSurveysInput {
    keyword: String
    state: SurveyState
  }

  extend type Query {
    isUniqueNameDuplicated(uniqueName: String, surveyId: ID): Boolean
    survey(id: ID, checkOrg: Boolean): Survey
    surveys(input: SurveysInput!): [Survey]
    surveysTotal(input: CountSurveysInput!): Int
    allSurveys: [Survey]
    getQuestionChartData(survey: ID, user: ID, questions: [ID]): JSON
    getDemoSurvey(demoName: String): Survey
    getStatsResults(jobGroupId: ID!, jobIds: [ID!]): JSON
    surveysPdf(id: ID, productIds: [String], questionFilters: [Object],isImpersonating:Boolean): String
    surveysTasterPdf(id: ID, surveyEnrollment: String): String
  }

  input CreateSurveyBasicsInput {
    name: String
    owner: ID!
    predecessorSurvey: ID
    products: [ID]
    state: String
    instructionsText: JSON
    instructionSteps: [String]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    uniqueName: String
    authorizationType: String
    exclusiveTasters: [String]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: JSON
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    recaptcha: Boolean
    minimumProducts: Int
    maximumProducts: Int
    surveyLanguage: String
    country: String
    customButtons: JSON
    savedRewards: JSON
    maxProductStatCount: Int
    autoAdvanceSettings: JSON
    pdfFooterSettings: JSON
    sharingButtons: Boolean
    validatedData: Boolean
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [ID]
    enabledEmailTypes: [String!]!
    emails: JSON
    showOnTasterDashboard: Boolean
  }

  input CreateSurveyInput {
    basics: CreateSurveyBasicsInput
    products: [ReplaceProductInput!]
    questions: [ReplaceQuestionInput!]
  }

  type CreateSurveyPayload {
    survey: Survey
    replacedProducts: [ReplaceProductPayload!]!
    replacedQuestions: [ReplaceQuestionPayload!]!
  }

  input UpdateSurveyBasicsInput {
    id: ID
    name: String
    instructionsText: JSON
    instructionSteps: [String]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    uniqueName: String
    authorizationType: String
    exclusiveTasters: [String]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: JSON
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    recaptcha: Boolean
    minimumProducts: Int
    maximumProducts: Int
    surveyLanguage: String
    country: String
    customButtons: JSON
    maxProductStatCount: Int
    autoAdvanceSettings: JSON
    pdfFooterSettings: JSON
    sharingButtons: Boolean
    validatedData: Boolean
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [ID]
    enabledEmailTypes: [String!]!
    emails: JSON
    showOnTasterDashboard: Boolean
  }

  input UpdateSurveyInput {
    basics: UpdateSurveyBasicsInput
    products: [ReplaceProductInput!]
    questions: [ReplaceQuestionInput!]
  }

  type UpdateSurveyPayload {
    survey: Survey
    replacedProducts: [ReplaceProductPayload!]!
    replacedQuestions: [ReplaceQuestionPayload!]!
  }

  input UpdateSurveyProductsInput {
    id: ID
    products: [ID]
  }

  extend type Mutation {
    createSurvey(input: CreateSurveyInput!): CreateSurveyPayload
    updateSurvey(input: UpdateSurveyInput!): UpdateSurveyPayload
    cloneSurvey(id: ID!): ID
    deprecateSurvey(id: ID!): ID
    suspendSurvey(id: ID!): Survey
    resumeSurvey(id: ID!): Survey
    updateSurveyBasics(input: UpdateSurveyBasicsInput): Survey
    updateSurveyProducts(input: UpdateSurveyProductsInput): Survey
    getSurveyShares(surveyId: ID!): [SurveyShares]
    getGiftCardShares(surveyId: ID!): [SurveyShares]
    getProductIncentives(surveyId: ID!): [SurveyShares]
    getGiftCardIncentives(surveyId: ID!): [SurveyShares]
    shareSurveyStatsScreen(surveyId: ID, users: [String]): Survey
    sendSurveyReport(surveyId: ID!, userId: ID!): Boolean
    sendSurveyPhotos(surveyId: ID!, userId: ID!): Boolean
  }
`

module.exports = Survey
