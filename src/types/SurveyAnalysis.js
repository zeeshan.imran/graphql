const SurveyAnalysis = /* GraphQL */ `
  type AnalysisFile {
    type: String
    name: String
  }

  type AnalysisResult {
    id: ID!
    name: String
    status: String!
    files: [AnalysisFile!]
    message: String
    description: String
  }

  extend type Query {
    findSurveyAnalysisResults(jobGroup: ID!): [AnalysisResult!]
    getAnalysisFile(id: ID!, file: String!): JSON
  }

  input CreateJobsInput {
    question: ID!
    analysisTypes: [String!]!
    referenceQuestion: ID
    productNames: JSON!
    questionCriteria: JSON
  }

  extend type Mutation {
    createJobs(input: CreateJobsInput!): String
  }
`

module.exports = SurveyAnalysis
