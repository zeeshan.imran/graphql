const SurveyEnrollment = /* GraphQL */ `
  type SurveyEnrollment {
    id: ID!
    survey: Survey
    state: String
    lastAnsweredQuestion: Question
    user: User
    selectedProducts: [Product]
    paypalEmail: String
    referral: ID
    savedRewards: JSON
    processed: Boolean
    validation: String
    comment: String
    browserInfo: JSON
    productDisplayOrder: [String]
    productDisplayType: String
  }

  type EnrollmentValidation {
    id: ID!
    paypalEmail: String
    processed: Boolean
    validation: String
    comment: String
    user: User
    answers: [Answer]
  }

  type ValidateSurveyEnrollement {
    total: Int
    enrollments: [EnrollmentValidation]
  }

  input EditValidateSurveyEnrollement {
    id: ID!
    surveyId: String
    processed: Boolean
    validation: String
    comment: String
  }

  type Funnel {
    responses: Int
    product: [FunnelProduct]
    question: Question
  }

  type SurveyFunnel {
    total: Int
    funnel: [Funnel]
  }

  input SubmitForcedSignUpInput {
    surveyEnrollment: ID!
    email: String!
    password: String
    country: String
  }

  input SubmitAnswerInput {
    question: ID!
    surveyEnrollment: ID!
    context: JSON
    value: JSON
    startedAt: String
    selectedProduct: ID
  }

  input TastingProfileDataInput {
    enrollment: ID
    survey: ID
    product: ID
    question: ID
    labels: [String]
    profileName: String
  }

  type ChartSeries {
    name: String
    data: [Float]
  }

  type TastingProfileData {
    labels: [String]
    series: [ChartSeries]
  }

  type SliderProfileData {
    labels: [String]
    series: [ChartSeries]
  }

  type UpdatedRecordCount {
    updatedRecords: Int
  }

  extend type Query {
    surveyEnrollment(id: ID): SurveyEnrollment
    surveyEnrollments: [SurveyEnrollment]
    tastingProfileData(input: TastingProfileDataInput!): TastingProfileData
    sliderProfileData(input: TastingProfileDataInput!): TastingProfileData
    surveyEnrollementValidation(surveyId: ID): ValidateSurveyEnrollement
    surveyFunnel(surveyId: ID, currentPage: Int, perPage: Int): SurveyFunnel
  }

  input EnrollmentStateInput {
    surveyEnrollment: String
  }


  extend type Mutation {
    saveRewards(rewards: JSON, surveyEnrollment: ID): Boolean
    submitAnswer(input: SubmitAnswerInput!): Boolean
    submitForcedSignUp(input: SubmitForcedSignUpInput!): Boolean
    setSelectedProducts(
      surveyEnrollment: ID!
      selectedProducts: [ID!]
    ): SurveyEnrollment
    setPaypalEmail(
      surveyEnrollment: ID!
      paypalEmail: String!
    ): SurveyEnrollment
    finishSurvey(input: EnrollmentStateInput): Boolean
    rejectSurvey(input: EnrollmentStateInput): Boolean
    finishSurveyScreening(input: EnrollmentStateInput): Boolean
    updateValidateSurveyEnrollement(input: EditValidateSurveyEnrollement): SurveyEnrollment
    updateSurveyEnrolmentProductIncentiveStatus(surveyID: ID, enrollments: [ID]): UpdatedRecordCount
    updateSurveyEnrolmentGiftCardIncentiveStatus(surveyID: ID, enrollments: [ID]): UpdatedRecordCount
    updateSurveyShareStatus(surveyID: ID, enrollments: [ID]): UpdatedRecordCount
  }
`

module.exports = SurveyEnrollment
