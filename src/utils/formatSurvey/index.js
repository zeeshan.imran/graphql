const { omit } = require('ramda')

const sortByOrder = (a, b) =>
  typeof a.order === 'number' && typeof b.order === 'number'
    ? a.order - b.order
    : 0

const formatSurvey = survey => {
  const { questions = [] } = survey
  const formattedSurvey = omit(['questions'], survey)

  const screeningQuestions = questions
    .filter(q => q.displayOn === 'screening')
    .sort(sortByOrder)

  const setupQuestions = questions
    .filter(q => q.displayOn === 'begin')
    .sort(sortByOrder)

  const productsQuestions = questions
    .filter(q => q.displayOn === 'middle')
    .sort(sortByOrder)

  const finishingQuestions = questions
    .filter(q => q.displayOn === 'end')
    .sort(sortByOrder)

  const paymentQuestions = questions
    .filter(q => q.displayOn === 'payments')
    .sort(sortByOrder)

  return {
    ...formattedSurvey,
    screeningQuestions,
    setupQuestions,
    productsQuestions,
    finishingQuestions,
    paymentQuestions
  }
}

module.exports = formatSurvey
