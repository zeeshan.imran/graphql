/* eslint-env jest */
const formatSurvey = require('.')

const mockApiSurvey = {
  id: 'abc',
  questions: [
    {
      displayOn: 'screening'
    },
    {
      displayOn: 'begin'
    },
    {
      displayOn: 'begin',
      prompt: 'second question in section'
    },
    {
      displayOn: 'end'
    },
    {
      displayOn: 'payments'
    }
  ],
  someOtherProp: 'untouched'
}

const mockApiSurveyWithOrder = {
  id: 'abc',
  questions: [
    {
      displayOn: 'screening',
      order: 3,
      prompt: 'ghi'
    },
    {
      displayOn: 'begin',
      prompt: 'abc',
      order: 1
    },
    {
      displayOn: 'begin',
      prompt: 'def',
      order: 0
    },
    {
      displayOn: 'end'
    },
    {
      displayOn: 'begin',
      prompt: 'last question in section'
    }
  ],
  someOtherProp: 'untouched'
}

describe('formatSurvey', () => {
  it('should format the incoming questions into separate question sections', () => {
    expect(formatSurvey(mockApiSurvey)).toMatchSnapshot()
  })

  it('should sort the incoming questions by their order property', () => {
    expect(formatSurvey(mockApiSurveyWithOrder)).toMatchSnapshot()
    expect(
      formatSurvey(mockApiSurveyWithOrder).setupQuestions[0].prompt
    ).toEqual('def')
    expect(
      formatSurvey(mockApiSurveyWithOrder).setupQuestions[1].prompt
    ).toEqual('abc')
  })

  it('should keep sorted questions also sorted by section, over order', () => {
    expect(formatSurvey(mockApiSurveyWithOrder)).toMatchSnapshot()
    expect(
      formatSurvey(mockApiSurveyWithOrder).screeningQuestions[0].prompt
    ).toEqual('ghi')
  })
})
