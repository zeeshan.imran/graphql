const getCombinationsWithCounter = pairs =>
  pairs.reduce((acc, pair, index) => {
    const entry = [pair.leftAttribute.pair, pair.rightAttribute.pair].sort().join('-')
    const counterSum = pair.leftAttributeCounter + pair.rightAttributeCounter

    let combination = acc[entry] || { permutations: [], counter: 0 }
    combination.permutations = [...combination.permutations, pair]
    combination.counter += counterSum

    return { ...acc, ...{ [entry]: combination } }
  }, {})

module.exports = getCombinationsWithCounter
