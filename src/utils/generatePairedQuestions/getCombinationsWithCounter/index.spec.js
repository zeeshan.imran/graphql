/* eslint-env jest */
const getCombinationsWithCounter = require('.')

const mockPairsWithoutAnswers = [
  {
    leftAttribute: { pair: 'A', image800: null },
    rightAttribute: { pair: 'B', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'B', image800: null },
    rightAttribute: { pair: 'A', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'A', image800: null },
    rightAttribute: { pair: 'C', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'C', image800: null },
    rightAttribute: { pair: 'A', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'B', image800: null },
    rightAttribute: { pair: 'C', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'C', image800: null },
    rightAttribute: { pair: 'B', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  }
]

const mockPairsWithAnsweredPairs = [
  {
    leftAttribute: { pair: 'A', image800: null },
    rightAttribute: { pair: 'B', image800: null },
    leftAttributeCounter: 2,
    rightAttributeCounter: 1
  },
  {
    leftAttribute: { pair: 'B', image800: null },
    rightAttribute: { pair: 'A', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'A', image800: null },
    rightAttribute: { pair: 'C', image800: null },
    leftAttributeCounter: 4,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'C', image800: null },
    rightAttribute: { pair: 'A', image800: null },
    leftAttributeCounter: 1,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'B', image800: null },
    rightAttribute: { pair: 'C', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  },
  {
    leftAttribute: { pair: 'C', image800: null },
    rightAttribute: { pair: 'B', image800: null },
    leftAttributeCounter: 0,
    rightAttributeCounter: 0
  }
]

it('should get the correct combinations for the initial pairs state', () => {
  const combinations = getCombinationsWithCounter(mockPairsWithoutAnswers)
  const indexes = Object.keys(combinations)
  expect(indexes).toEqual(['A-B', 'A-C', 'B-C'])
  indexes.forEach(index => expect(combinations[index].counter).toEqual(0))
  expect(combinations['A-B'].permutations.length).toEqual(2)
  expect(combinations['A-B'].permutations[0].leftAttribute).toEqual(
    combinations['A-B'].permutations[1].rightAttribute
  )
  expect(combinations['A-B'].permutations[0].rightAttribute).toEqual(
    combinations['A-B'].permutations[1].leftAttribute
  )
  expect(combinations['A-C'].permutations.length).toEqual(2)
  expect(combinations['A-C'].permutations[0].leftAttribute).toEqual(
    combinations['A-C'].permutations[1].rightAttribute
  )
  expect(combinations['A-C'].permutations[0].rightAttribute).toEqual(
    combinations['A-C'].permutations[1].leftAttribute
  )
  expect(combinations['B-C'].permutations.length).toEqual(2)
  expect(combinations['B-C'].permutations[0].leftAttribute).toEqual(
    combinations['B-C'].permutations[1].rightAttribute
  )
  expect(combinations['B-C'].permutations[0].rightAttribute).toEqual(
    combinations['B-C'].permutations[1].leftAttribute
  )
})

it('should get the correct combinations for pairs that have already been answered', () => {
  const combinations = getCombinationsWithCounter(mockPairsWithAnsweredPairs)

  expect(combinations['A-B'].counter).toEqual(3)
  expect(combinations['A-C'].counter).toEqual(5)
  expect(combinations['B-C'].counter).toEqual(0)
})

it('should match snapshot', () => {
  expect(getCombinationsWithCounter(mockPairsWithoutAnswers)).toMatchSnapshot()
  expect(
    getCombinationsWithCounter(mockPairsWithAnsweredPairs)
  ).toMatchSnapshot()
})
