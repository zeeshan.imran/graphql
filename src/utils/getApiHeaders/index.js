const getApiHeaders = headers => ({
  Authorization: headers.Authorization || headers.authorization,
  Impersonate: headers.Impersonate || headers.impersonate
})

module.exports = getApiHeaders
