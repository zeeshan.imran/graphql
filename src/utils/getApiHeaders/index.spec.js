/* eslint-env jest */
const getApiHeaders = require('./index')

describe('get headers that need to pass into api', () => {
  test('should support Authorization', () => {
    const apiHeaders = getApiHeaders({
      Authorization: 'Bearer token'
    })

    expect(apiHeaders.Authorization).toBe('Bearer token')
  })

  test('should support authorization', () => {
    const apiHeaders = getApiHeaders({
      authorization: 'Bearer token'
    })

    expect(apiHeaders.Authorization).toBe('Bearer token')
  })

  test('should support Impersonate', () => {
    const apiHeaders = getApiHeaders({
      Impersonate: 'impersonate_user_id'
    })

    expect(apiHeaders.Impersonate).toBe('impersonate_user_id')
  })

  test('should support impersonate', () => {
    const apiHeaders = getApiHeaders({
      impersonate: 'impersonate_user_id'
    })

    expect(apiHeaders.Impersonate).toBe('impersonate_user_id')
  })
})
