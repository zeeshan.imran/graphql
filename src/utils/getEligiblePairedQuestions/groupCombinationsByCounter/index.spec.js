/* eslint-env jest */
const groupCombinationsByCounter = require('.')

const mockCombinations = {
  abc: { counter: 1 },
  def: { counter: 0 },
  ghi: { counter: 1 },
  jkl: { counter: 5 }
}

it('should have one element in the 0 counts group', () => {
  expect(groupCombinationsByCounter(mockCombinations)[0].length).toEqual(1)
})

it('should have two elements in the 1 count group', () => {
  expect(groupCombinationsByCounter(mockCombinations)[1].length).toEqual(2)
})

it('should not have a 2, 3, and 4 count group', () => {
  expect(groupCombinationsByCounter(mockCombinations)[2]).toBeUndefined()
  expect(groupCombinationsByCounter(mockCombinations)[3]).toBeUndefined()
  expect(groupCombinationsByCounter(mockCombinations)[4]).toBeUndefined()
})

it('should have one element in the 5 count group', () => {
  expect(groupCombinationsByCounter(mockCombinations)[5].length).toEqual(1)
})

it('should match snapshot', () => {
  expect(groupCombinationsByCounter(mockCombinations)).toMatchSnapshot()
})
