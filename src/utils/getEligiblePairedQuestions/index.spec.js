/* eslint-env jest */
const getEligiblePairedQuestions = require('.')

const mockForEqualCounter = [['A-B', 'A-C', 'B-C']]

const mockWithUnansweredQuestions = [['A-B'], ['A-C'], ['B-C']]

const mockWithEmptyCounter = [['A-B'], undefined, undefined, ['A-C', 'B-C']]

jest.mock('./groupCombinationsByCounter', () => jest.fn(a => a))

it('should return all the indexes even with MIN_PAIRS equal to 1', () => {
  expect(getEligiblePairedQuestions(mockForEqualCounter, 1)).toEqual([
    'A-B',
    'A-C',
    'B-C'
  ])
})

it('should return all the indexes with MIN_PAIRS equal to 3', () => {
  expect(getEligiblePairedQuestions(mockForEqualCounter, 3)).toEqual([
    'A-B',
    'A-C',
    'B-C'
  ])
})

it('should return the pairs to achieve balance in the answers', () => {
  expect(getEligiblePairedQuestions(mockWithUnansweredQuestions, 1)).toEqual([
    'A-B',
    'A-C'
  ])
})

it('should return the pairs forcing the third element to be included', () => {
  expect(getEligiblePairedQuestions(mockWithUnansweredQuestions, 3)).toEqual([
    'A-B',
    'A-C',
    'B-C'
  ])
})

it('should return one pair', () => {
  expect(getEligiblePairedQuestions(mockWithEmptyCounter, 1)).toEqual(['A-B'])
})

it('should match snapshot', () => {
  expect(getEligiblePairedQuestions(mockForEqualCounter, 2)).toMatchSnapshot()
  expect(
    getEligiblePairedQuestions(mockWithUnansweredQuestions, 2)
  ).toMatchSnapshot()
  expect(getEligiblePairedQuestions(mockWithEmptyCounter, 2)).toMatchSnapshot()
})
